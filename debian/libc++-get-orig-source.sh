#!/bin/sh

if test ! -d debian; then
    echo "This script must be started from the base directory"
    exit 1
fi

if test $# -ne 1 -a $# -ne 2; then
    echo "Version is expected"
    echo "Example: sh debian/libc++-get-orig-source.sh 3.6.2 RELEASE_362/final/"
    exit 1
fi

PACKAGE=libc++
SRC_VERSION=$1
SVN_REVISION=`echo ${SRC_VERSION} | awk -F'~' '{ print $2 }' | sed 's/svn//'`
if test $# -eq 2; then
    SRC_VERSION=$1
    TAG=$2
    LIBCXX_SVN=http://llvm.org/svn/llvm-project/libcxx/tags/$TAG
    LIBCXXABI_SVN=http://llvm.org/svn/llvm-project/libcxxabi/tags/$TAG
else
    LIBCXX_SVN=http://llvm.org/svn/llvm-project/libcxx/trunk
    LIBCXXABI_SVN=http://llvm.org/svn/llvm-project/libcxxabi/trunk
fi
echo "Using $LIBCXX_SVN and $LIBCXXABI_SVN for checkout"

if [ -z "$SVN_REVISION" -a -z "$TAG" ]; then
	echo "No SVN revision specified, using current revision..."
	SVN_REVISION=`LANG=C svn info $LIBCXX_SVN| sed -ne 's/Revision: \([0-9]*\)/\1/p'`
	echo "Current revision is $SVN_REVISION."
	SRC_VERSION="${SRC_VERSION}~svn${SVN_REVISION}"
else
	echo "Using SVN revision ${SVN_REVISION}..."
	SVN_VERSION="${SRC_VERSION}"
fi

PKG_NAME=${PACKAGE}_${SRC_VERSION}
TARBALL=${PKG_NAME}.orig.tar.bz2

rm -rf tmp $TARBALL

mkdir tmp

if [ -n "$SVN_REVISION" ]; then
    svn co -r $SVN_REVISION $LIBCXX_SVN tmp/libcxx
    svn co -r $SVN_REVISION $LIBCXXABI_SVN tmp/libcxxabi
else
    svn co $LIBCXX_SVN tmp/libcxx
    svn co $LIBCXXABI_SVN tmp/libcxxabi
fi

GZIP='--best --no-name' tar cjf $TARBALL --exclude-vcs -C tmp libcxx libcxxabi

rm -rf tmp

echo "$TARBALL created; move it to the right destination to build the package"
